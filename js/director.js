$(document).ready(function(){
    $(".animsition").animsition({
        inClass: 'fade-in-up-sm',
        outClass: 'fade-out',
        inDuration: 1500,
        outDuration: 800,
        linkElement: 'a:not([target="_blank"]):not([href^="#"])',
        loading: true,
        loadingParentElement: 'html', //animsition wrapper element
        loadingClass: 'animsition-loading',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url;}
    });
    var mySwiper = new Swiper('.swiper-container1', {
          initialSlide: 0,
          direction: 'horizontal',
          speed: 500,
          grabCursor: false,
          centeredSlides: true,
          slidesPerView: 1,
          breakpoints: {
          960: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          600: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          480: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          320: {
            slidesPerView: 1,
            spaceBetween: 0
          }
          },
          pagination: '.swiper-pagination',
          paginationClickable: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          //autoplay: 3000,
          autoplayStopOnLast: false,
          autoplayDisableOnInteraction: false,
          freeMode: false,
          freeModeMomentum: false,
          freeModeMomentumRatio: 1,
          freeModeMomentumVelocityRatio: 1,
          freeModeMomentumBounce: false,
          freeModeMomentumBounceRatio: 1,
          freeModeMinimumVelocity: 0.02,
          freeModeSticky: false,
          effect: 'fade',
          fade: {
          crossFade: false
          },
          cube: {
            slideShadows: false,
            shadow: false,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 20,
            modifier: 1,
            slideShadows : true
          },
          flip: {
            slideShadows : true,
            limitRotation: true
          },
          spaceBetween: 0,
          centeredSlides: false,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0,
          touchRatio: 1,
          touchAngle: 45,
          simulateTouch: true,
          shortSwipes: true,
          longSwipes: true,
          longSwipesRatio: 0.5,
          longSwipesMs: 300,
          followFinger: true,
          onlyExternal: false,
          threshold: 0,
          touchMoveStopPropagation: true,
          iOSEdgeSwipeDetection: false,
          iOSEdgeSwipeThreshold: 20,
          touchReleaseOnEdges: false,
          passiveListeners: true,
          resistance: true,
          resistanceRatio: 0.85,
          slideToClickedSlide: false,
          allowSwipeToPrev: true,
          allowSwipeToNext: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          paginationType: 'bullets',
          keyboardControl: false,
          mousewheelControl: false,
          mousewheelForceToAxis: false,
          mousewheelReleaseOnEdges: false,
          mousewheelInvert: false,
          mousewheelSensitivity: 1,
          preloadImages: true,
          updateOnImagesReady: true,
          loop: true
      });

    $('#videoModal').on('hidden.bs.modal', function () {
        $('#videoModal iframe').removeAttr('src');
    });

    new ModalVideo('.video-modal', {channel: 'vimeo'});
});