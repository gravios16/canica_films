$(document).ready(function(){

    $elmt = $("body").vegas({
      slide: 0,
      preload: true,
      preloadImage: false,
      preloadVideo: true,
      delay: 5000,
      timer: false,
      overlay: false,
      autoplay: false,
      loop: true,
      shuffle: false,
      cover: true,
      align: 'center',
      valign: 'center',
      transition: 'fade',
      transitionDuration: 2000,
      firstTransition: 'fade',
      firstTransitionDuration: 2000,
      animation: 'kenburns',
      animationDuration: 2000,
      slides: [
        {src:'assets/videoproduccion1.jpg',
        video:{src:['assets/videoproduccion1.mp4','assets/videoproduccion1.webm','assets/videoproduccion1.ogv'],loop:true,mute:true},
        },
        {src:'assets/videoproduccion2.jpg',
        video:{src:['assets/videoproduccion2.mp4','assets/videoproduccion2.webm','assets/videoproduccion2.ogv'],loop:true,mute:true},
        },
        {src:'assets/videoproduccion3.jpg',
        video:{src:['assets/videoproduccion3.mp4','assets/videoproduccion3.webm','assets/videoproduccion3.ogv'],loop:true,mute:true},
        },
        {src:'assets/videoproduccion4.jpg',
        video:{src:['assets/videoproduccion4.mp4','assets/videoproduccion4.webm','assets/videoproduccion4.ogv'],loop:true,mute:true},
        }
      ]
    });

    $(".animsition").animsition({
        inClass: 'fade-in-up-sm',
        outClass: 'fade-out',
        inDuration: 1500,
        outDuration: 800,
        linkElement: 'a:not([target="_blank"]):not([href^="#"])',
        loading: true,
        loadingParentElement: 'html', //animsition wrapper element
        loadingClass: 'animsition-loading',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url;}
    });


  var mySwiper2 = new Swiper('.swiper-container1.swiper2', {
      initialSlide: 0,
      direction: 'horizontal',
      speed: 1000,
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 3,
      breakpoints: {
      960: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      600: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      480: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 0
      }
      },
      pagination: '.swiper-pagination',
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      //autoplay: 2000,
      autoplayStopOnLast: false,
      autoplayDisableOnInteraction: false,
      freeMode: false,
      freeModeMomentum: false,
      freeModeMomentumRatio: 1,
      freeModeMomentumVelocityRatio: 1,
      freeModeMomentumBounce: false,
      freeModeMomentumBounceRatio: 1,
      freeModeMinimumVelocity: 0.02,
      freeModeSticky: false,
      effect: 'slide',
      fade: {
      crossFade: false
      },
      cube: {
        slideShadows: false,
        shadow: false,
        shadowOffset: 20,
        shadowScale: 0.94
      },
      coverflow: {
        rotate: 50,
        stretch: 0,
        depth: 20,
        modifier: 1,
        slideShadows : true
      },
      flip: {
        slideShadows : true,
        limitRotation: true
      },
      spaceBetween: 0,
      centeredSlides: false,
      slidesOffsetBefore: 0,
      slidesOffsetAfter: 0,
      touchRatio: 1,
      touchAngle: 45,
      simulateTouch: true,
      shortSwipes: true,
      longSwipes: true,
      longSwipesRatio: 0.5,
      longSwipesMs: 300,
      followFinger: true,
      onlyExternal: false,
      threshold: 0,
      touchMoveStopPropagation: true,
      iOSEdgeSwipeDetection: false,
      iOSEdgeSwipeThreshold: 20,
      touchReleaseOnEdges: false,
      passiveListeners: true,
      resistance: true,
      resistanceRatio: 0.85,
      slideToClickedSlide: false,
      allowSwipeToPrev: true,
      allowSwipeToNext: true,
       nextButton: '.next',
       prevButton: '.prev',
      paginationType: 'bullets',
      keyboardControl: false,
      mousewheelControl: false,
      mousewheelForceToAxis: false,
      mousewheelReleaseOnEdges: false,
      mousewheelInvert: false,
      mousewheelSensitivity: 1,
      preloadImages: true,
      updateOnImagesReady: true,
      loop: true
  });

  var tooltipConf = {
      position: 'bottom',
      trigger: 'mouseenter',
      interactive: false,
      delay: 0,
      animation: 'shift',
      arrow: true,
      arrowSize: 'regular',
      animateFill: false,
      duration: 400,
      hideDuration: 0,
      size: 'regular',
      theme: 'theme1',
      distance: 10,
      offset: 0,
      hideOnClick: true,
      multiple: true,
      followCursor: false,
      inertia: false,
      theme: "cf"
  };

  tippy('.tooltip1', tooltipConf);
  tippy('.tooltip2', tooltipConf);
  tippy('.tooltip3', tooltipConf);
  tippy('.tooltip4', tooltipConf);
  tippy('.tooltip5', tooltipConf);
  tippy('.tooltip6', tooltipConf);


   var mySwiper1 = new Swiper('.swiper-container1.swiper1', {
          initialSlide: 0,
          direction: 'horizontal',
          speed: 0,
          grabCursor: false,
          centeredSlides: true,
          slidesPerView: 1,
          breakpoints: {
          960: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          600: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          480: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          320: {
            slidesPerView: 1,
            spaceBetween: 0
          }
          },
          pagination: '.swiper-pagination',
          paginationClickable: false,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          //autoplay: 3000,
          autoplayStopOnLast: false,
          autoplayDisableOnInteraction: false,
          freeMode: false,
          freeModeMomentum: false,
          freeModeMomentumRatio: 1,
          freeModeMomentumVelocityRatio: 1,
          freeModeMomentumBounce: false,
          freeModeMomentumBounceRatio: 1,
          freeModeMinimumVelocity: 0.02,
          freeModeSticky: false,
          effect: 'fade',
          fade: {
          crossFade: false
          },
          cube: {
            slideShadows: false,
            shadow: false,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 20,
            modifier: 1,
            slideShadows : true
          },
          flip: {
            slideShadows : true,
            limitRotation: true
          },
          spaceBetween: 0,
          centeredSlides: false,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0,
          touchRatio: 1,
          touchAngle: 45,
          simulateTouch: true,
          shortSwipes: true,
          longSwipes: true,
          longSwipesRatio: 0.5,
          longSwipesMs: 300,
          followFinger: true,
          onlyExternal: false,
          threshold: 0,
          touchMoveStopPropagation: true,
          iOSEdgeSwipeDetection: false,
          iOSEdgeSwipeThreshold: 20,
          touchReleaseOnEdges: false,
          passiveListeners: true,
          resistance: true,
          resistanceRatio: 0.85,
          slideToClickedSlide: false,
          allowSwipeToPrev: true,
          allowSwipeToNext: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          paginationType: 'bullets',
          keyboardControl: false,
          mousewheelControl: false,
          mousewheelForceToAxis: false,
          mousewheelReleaseOnEdges: false,
          mousewheelInvert: false,
          mousewheelSensitivity: 1,
          preloadImages: true,
          updateOnImagesReady: true,
          loop: true
      });

   $(".swiper-button-next").on('click', function() {
      $elmt.vegas("next");
    });

    $(".swiper-button-prev").on('click', function() {
      $elmt.vegas("previous");
    });
});