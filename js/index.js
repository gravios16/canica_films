$(document).ready(function(){
    $elmt = $("body").vegas({
      slide: 0,
      preload: true,
      preloadImage: false,
      preloadVideo: true,
      delay: 5000,
      timer: false,
      overlay: false,
      autoplay: false,
      loop: true,
      shuffle: false,
      cover: true,
      align: 'center',
      valign: 'center',
      transition: 'fade',
      transitionDuration: 2000,
      firstTransition: 'fade',
      firstTransitionDuration: 2000,
      animation: 'kenburns',
      animationDuration: 2000,
      slides: [
       {src:'assets/indeximagen1.jpg',
       video:{src:['assets/indexvideo01.mp4','assets/indexvideo01.webm','assets/indexvideo01.ogv'],loop:true,mute:true},
       },
       {src:'assets/indeximagen2.jpg',
       video:{src:['assets/indexvideo02.mp4','assets/indexvideo02.webm','assets/indexvideo02.ogv'],loop:true,mute:true},
       },
       {src:'assets/indeximagen3.jpg',
       video:{src:['assets/indexvideo03.mp4','assets/indexvideo03.webm','assets/indexvideo03.ogv'],loop:true,mute:true},
       },
       {src:'assets/indeximagen4.jpg',
       video:{src:['assets/indexvideo04.mp4','assets/indexvideo04.webm','assets/indexvideo04.ogv'],loop:true,mute:true},
       },
       {src:'assets/indeximagen5.jpg',
       video:{src:['assets/indexvideo05.mp4','assets/indexvideo05.webm','assets/indexvideo05.ogv'],loop:true,mute:true},
       }
      ]
    });

    var mySwiper = new Swiper('.swiper-container1', {
          initialSlide: 0,
          direction: 'horizontal',
          speed: 5000,
          grabCursor: false,
          centeredSlides: true,
          slidesPerView: 1,
          breakpoints: {
          960: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          600: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          480: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          320: {
            slidesPerView: 1,
            spaceBetween: 0
          }
          },
          pagination: '.swiper-pagination',
          paginationClickable: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          autoplay: 5000,
          autoplayStopOnLast: false,
          autoplayDisableOnInteraction: false,
          freeMode: false,
          freeModeMomentum: false,
          freeModeMomentumRatio: 1,
          freeModeMomentumVelocityRatio: 1,
          freeModeMomentumBounce: false,
          freeModeMomentumBounceRatio: 1,
          freeModeMinimumVelocity: 0.02,
          freeModeSticky: false,
          effect: 'fade',
          fade: {
          crossFade: false
          },
          cube: {
            slideShadows: false,
            shadow: false,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 20,
            modifier: 1,
            slideShadows : true
          },
          flip: {
            slideShadows : true,
            limitRotation: true
          },
          spaceBetween: 0,
          centeredSlides: false,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0,
          touchRatio: 1,
          touchAngle: 45,
          simulateTouch: true,
          shortSwipes: true,
          longSwipes: true,
          longSwipesRatio: 0.5,
          longSwipesMs: 300,
          followFinger: true,
          onlyExternal: false,
          threshold: 0,
          touchMoveStopPropagation: true,
          iOSEdgeSwipeDetection: false,
          iOSEdgeSwipeThreshold: 20,
          touchReleaseOnEdges: false,
          passiveListeners: true,
          resistance: true,
          resistanceRatio: 0.85,
          slideToClickedSlide: false,
          allowSwipeToPrev: true,
          allowSwipeToNext: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          paginationType: 'bullets',
          keyboardControl: false,
          mousewheelControl: false,
          mousewheelForceToAxis: false,
          mousewheelReleaseOnEdges: false,
          mousewheelInvert: false,
          mousewheelSensitivity: 1,
          preloadImages: true,
          updateOnImagesReady: true,
          loop: true
      });

    mySwiper.on('slideNextStart', function () {
        $elmt.vegas("next");
    });

    mySwiper.on('slidePrevStart', function () {
        $elmt.vegas("previous");
    });

    $(".animsition").animsition({
        inClass: 'fade-in-up-sm',
        outClass: 'fade-out',
        inDuration: 1500,
        outDuration: 800,
        linkElement: 'a:not([target="_blank"]):not([href^="#"])',
        loading: true,
        loadingParentElement: 'html', //animsition wrapper element
        loadingClass: 'animsition-loading',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url;}
    });



    var inf_scroll_flag = true;

    $(window).scroll(function() {
        // Fetch variables
        var scrollTop = $(document).scrollTop();
        var windowHeight = $(window).height();
        var bodyHeight = $(document).height() - windowHeight;
        var scrollPercentage = (scrollTop / bodyHeight);

        // if the scroll is more than 90% from the top, load more content.
        if(scrollPercentage > 0.9) {
          if (inf_scroll_flag){
            loadContent();
          }
        }
    });

    function loadContent(){
      inf_scroll_flag = false;
      $(".inf-scroll").append('<div class="loading-inf"><div class="loader"></div></div>');


      //eso simularia el ajax que utilices, esta parte iria en el success.
      setTimeout(function() {



        var html_content = '<div class="row m-0"><div class="col-sm px-0 bg-light"><a href="newsfeed.html" class="no-decoration"><div class="group-link-box align-items-center justify-content-center d-flex" style="background-image: url(img/newsfeed/newsfeed1.jpg);"><span class="px-2">MUEBLE, OBJETOVIVO</span></div></a></div><div class="col-sm px-0 bg-light"><a href="index.html"class="no-decoration"><div class="group-link-box align-items-center justify-content-centerd-flex" style="background-image:url(img/newsfeed/newsfeed2.jpg);"><span class="px-2">MASTERCARD</span></div></a></div><div class="col-sm px-0 bg-light"><a href="index.html" class="no-decoration"><div class="group-link-box align-items-center justify-content-center d-flex" style="background-image:url(img/newsfeed/newsfeed3.jpg);"><span class="px-2">COCACOLA</span></div></a></div></div>';

        //Esto debe ir en el succes de tu AJAX
            $(".loading-inf").remove();
            $(".inf-scroll").append(html_content);
            inf_scroll_flag = true;
        //-------------------------------------




      }, 1500);

    }
});